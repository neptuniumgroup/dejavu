from distutils.core import setup

setup(
    name='Dejavu',
    version='0.0.1',
    author='Will Drevo',
    author_email='will [ dot ] drevo [ at ] gmail [ dot ] com',
    packages=['dejavu'],
    scripts=['bin/go.py'],
    url='https://neptuniumgroup@bitbucket.org/neptuniumgroup/dejavu.git',
    license='LICENSE.md',
    description='Audio fingerprinting and recognition in Python',
    long_description=open('README.md').read(),
    install_requires=[
        'pyaudio',
        'pydub',
        'numpy',
        'scipy',
        'matplotlib',
        'psycopg2'
    ],
)
